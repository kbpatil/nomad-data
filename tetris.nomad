job "tetris" {
  datacenters = ["dc1"]

  group "games" {
    count = 5
    network {
      mode = "host"
      port "http" {
        to = 80
      }
    }
    spread {
      attribute = "${attr.platform.aws.placement.availability-zone}" # added attribute from UI cliet overview section
      weight = 100 # consider all task as 100 percent
      
      target = "us-east-1b" {
        percent = 100  # distribute 100 percent tasks on client running in us-east-1b zone
      }
      
      target = "us-east-1c" {
        percent = 0
      }
    }
    task "tetris" {
      driver = "docker"
      config {
        image = "bsord/tetris"
        ports = ["http"]
      }
      resources {
        cpu    = 50
        memory = 50
      }
    }
  }
}